package com.afs.restapi.service;

import com.afs.restapi.dto.EmployeeCreateRequest;
import com.afs.restapi.dto.EmployeeResponse;
import com.afs.restapi.dto.EmployeeUpdateRequest;
import com.afs.restapi.entity.Employee;
import com.afs.restapi.exception.EmployeeNotFoundException;
import com.afs.restapi.mapper.EmployeeMapper;
import com.afs.restapi.repository.EmployeeRepository;
import org.springframework.data.domain.PageRequest;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

import static com.afs.restapi.mapper.EmployeeMapper.toResponse;

@Service
public class EmployeeService {

    EmployeeRepository employeeRepository;

    public EmployeeService(EmployeeRepository employeeRepository) {
        this.employeeRepository = employeeRepository;
    }

    public List<EmployeeResponse> findAll() {
        return employeeRepository.findAllByStatusTrue().stream()
                .map(EmployeeMapper::toResponse)
                .collect(Collectors.toList());
    }

    public Employee update(int id, EmployeeUpdateRequest toUpdate) {
        return employeeRepository.findByIdAndStatusTrue(id)
                .map(employee -> {
                    if (toUpdate.getAge() != null) {
                        employee.setAge(toUpdate.getAge());
                    }
                    if (toUpdate.getSalary() != null) {
                        employee.setSalary(toUpdate.getSalary());
                    }
                    employeeRepository.save(employee);
                    return employee;
                }).orElseThrow(EmployeeNotFoundException::new);
    }

    public EmployeeResponse findById(int id) {
        return employeeRepository.findByIdAndStatusTrue(id)
                .map(EmployeeMapper::toResponse)
                .orElseThrow(EmployeeNotFoundException::new);
    }

    public List<EmployeeResponse> findByGender(String gender) {
        return employeeRepository.findByGenderAndStatusTrue(gender).stream()
                .map(EmployeeMapper::toResponse)
                .collect(Collectors.toList());
    }

    public List<EmployeeResponse> findByPage(int pageNumber, int pageSize) {
        PageRequest pageRequest = PageRequest.of(pageNumber, pageSize);
        return employeeRepository.findAllByStatusTrue(pageRequest).get()
                .map(EmployeeMapper::toResponse)
                .collect(Collectors.toList());
    }

    public Employee insert(EmployeeCreateRequest employeeCreateRequest) {
        Employee employee = EmployeeMapper.toEntity(employeeCreateRequest);
        employee.setStatus(true);
        return employeeRepository.save(employee);
    }

    public void delete(int id) {
        employeeRepository.findById(id)
                .map(employee -> {
                    employee.setStatus(false);
                    employeeRepository.save(employee);
                    return employee;
                }).orElseThrow(EmployeeNotFoundException::new);
    }
}
