package com.afs.restapi.mapper;

import com.afs.restapi.dto.CompanyCreateRequest;
import com.afs.restapi.entity.Company;
import org.springframework.beans.BeanUtils;

public class CompanyMapper {

    public static Company toEntity(CompanyCreateRequest companyCreateRequest){
        Company company = new Company();
        BeanUtils.copyProperties(companyCreateRequest,company);
        return company;
    }
}
